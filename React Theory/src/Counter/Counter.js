import React, {Component} from "react";
import Auxiliary from "../hoc/Auxillary";
import Counter2 from "../Counter2/Counter2";

export default class Counter extends Component {
    
    state = {
        counter: 0
    }

    addCounter = () => {
        //Синхронный способ
        /* this.setState({
            counter: this.state.counter + 1
        }) */

        //Асинхронный способ, способ лучше
        this.setState((prevState) => {
            return {
                counter: prevState.counter + 1
            };
        })
    }

    render() {
        /* return (
            <div>
                <h2>Counter {this.state.counter}</h2>
                <button onClick={this.addCounter}>+</button>
                <button onClick={() => this.setState({counter: this.state.counter - 1})}>-</button>
            </div>
        ) */

        //Вот так можно возвращать код в return без <div> - т.е без корневого элемента. т.е теперь будет возвращаться массив.
        //Нужно еще каждому элементу массива задать атрибут key, чтобы в React не было ошибок. Но этот подход не очень.
        /* return [
            <h2 key={"1"}>Counter {this.state.counter}</h2>,
            <button key={"2"} onClick={this.addCounter}>+</button>,
            <button key={"3"} onClick={() => this.setState({counter: this.state.counter - 1})}>-</button>
        ] */

        //А вот этот подход очень, его и используют.
        //А также есть короткий синтаксис для записи React.Fragment такой как пустой тег <> и закрытый </>
        /* return (
            <React.Fragment>
                <h2>Counter {this.state.counter}</h2>
                <button onClick={this.addCounter}>+</button>
                <button onClick={() => this.setState({counter: this.state.counter - 1})}>-</button>
            </React.Fragment>
        ) */

        //А можно встретить и такой подход, это когда мы создаем свои собественные фрагменты.
        return (
            <Auxiliary>
                <h2>Counter {this.state.counter}</h2>
                <Counter2 />
                <button onClick={this.addCounter}>+</button>
                <button onClick={() => this.setState({counter: this.state.counter - 1})}>-</button>
            </Auxiliary>
        )
    }
}