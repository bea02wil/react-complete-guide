import logo from './logo.svg';
import './App.css';
import Car from "./Car/Car";
import ErrorBoundary from "./ErrorBoundary/ErrorBoundary";
import Counter from "./Counter/Counter";
import React, { Component } from 'react';

export const ClickedContext = React.createContext(false);

//метод render() пишется внизу по удобству чтения
//state заполняется вверху
class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      clicked: false,
      cars: [
        {name: "Ford", year: 2018},
        {name: "Audi", year: 2016},
        {name: "Mazda 1", year: 2010}
      ],
      pageTitle: "React components",
      showCars: false
    };
  }

  //Еще 1 способ задания state
  // state = {
  //   cars: [
  //     {name: "Ford", year: 2018},
  //     {name: "Audi", year: 2016},
  //     {name: "Mazda 1", year: 2010}
  //   ],
  //   pageTitle: "React components",
  //   showCars: false
  // };
   
  changeTitleHandler = (newTitle) => {
     
    this.setState({
      pageTitle: newTitle
    });

  }

  // Так тоже можно. Т.е. когда совпадает имя параметра с именем ключа свойства
  /* changeTitleHandler = pageTitle => {
     
    this.setState({pageTitle});
  } */

  onChangeName = (name, index) => {
    const car = this.state.cars[index];
    car.name = name;

    //копируем сюда старый массив
    const cars = [...this.state.cars];

    cars[index] = car;
    this.setState({
      //если совпадают ключ и значение, можно просто написать было cars
      cars: cars
    });
  }

  toggleCarsHandler = () => {
    this.setState({
      showCars: !this.state.showCars
    });
  }

  handleInput = (event) => {
    this.setState({
      pageTitle: event.target.value
    });
  }

  deleteHandler = (index) => {
    //или так копируем старый массив
    const cars = this.state.cars.concat();

    //Можно так удалить элемент массива, с индекса и сколько элементов
    cars.splice(index, 1);

    //как раз-таки пример, когда совпадают и ключ и значение
    this.setState({cars});
  }

  //Функции жизненного цикла реакт компонента (включая render() ). Mount в переводе как внедрение, т.е тоже что и Render слово.
  //То есть что-то положить в HTML или DOM-дерево
  componentWillMount() {
    console.log("App componentWillMount");
  }

  componentDidMount() {
    console.log("App componentDidMount");
  }

  render() {

    console.log("App render");

    const divStyle = {
      textAlign: "center"
    };

    let cars = null;

    if (this.state.showCars) {
      cars = this.state.cars.map((car, index) => {
        return (
          <ErrorBoundary key={index}>
            <Car              
              name={car.name}
              year={car.year}
              index={index}
              // onChangeTitle={this.changeTitleHandler.bind(this, car.name)}

              //event приходит из самого реакта в функции, из которой экспортируем Car
              onChangeName={event => this.onChangeName(event.target.value, index)}

              //Если deleteHandler объявлен как стрелочная функция, то можно передать и так, не байндив
              onDelete={this.deleteHandler}
            />
          </ErrorBoundary>
        )
    })
    }

    return (
      <div style={divStyle}>
         {/* <h1>{this.state.pageTitle}</h1> */}
          <h1>{this.props.title}</h1>

          <ClickedContext.Provider value={this.state.clicked}>
            <Counter />
          </ClickedContext.Provider>
          
          <hr/>
         {/* <input type="text" onChange={this.handleInput}></input> */}

         {/* <button onClick={this.changeTitleHandler.bind(this, "Changed!")}>Change title</button> */}

         <button style={{marginTop: 20}} className={"AppButton"} onClick={this.toggleCarsHandler}>TOGGLE CARS</button>

         <button onClick={() => this.setState({clicked: true})}>Change clicked</button>

         <div style={{width: 400, margin: "auto", paddingTop: "20px"}}>
            {cars}
         </div>
         



          {/* Можно использовать и так, но уже с помощью тернарного оператора, if, for и подобные не работают в return */}
          {/* { this.state.showCars ? 
              this.state.cars.map((car, index) => {
                return (
                  <Car
                    key={index} 
                    name={car.name}
                    year={car.year}
                    onChangeTitle={this.changeTitleHandler.bind(this, car.name)}
                  />
                )
          }) : null }; */}

          

         {/* Сверху запись аналогична этой */}
         {/* <Car 
            name={cars[0].name}
            year={cars[0].year}
            onChangeTitle={this.changeTitleHandler.bind(this, cars[0].name)} />
         <Car 
            name={cars[1].name}
            year={cars[1].year}
            onChangeTitle={() => this.changeTitleHandler(cars[1].name)} />
         <Car 
            name={cars[2].name}
            year={cars[2].year}
            onChangeTitle={() => this.changeTitleHandler(cars[2].name)} /> */}

         
      </div>
    );

  }
}

export default App;
