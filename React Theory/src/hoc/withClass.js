import React from "react";

const withClass = (Component, className) => {
    return (props) => {
        return (
            <div className={className}>

                {/* чтобы развернул объект props. Здесь должны сами сделать разворот */}
                <Component {...props} /> 
            </div>
        )
    }
}

export default withClass;