import React, { Component } from "react";
//компомент можно было бы не импортировать, а написать в классе extends React.Component, это тоже самое
import "./Car.css";
import Radium from "radium";
import PropTypes from "prop-types";
import withClass from "../hoc/withClass";

//Можно так создавать компонент (будет доп контент от класса Component)
/* class Car extends Component {
    render() {
        return (
            <div>
                <h3>Car name: {"CAR_NAME"} </h3>
                <p>Year: <strong>{2018}</strong></p>
            </div>
        )
    }
} */

//Или:
// const car = () => <div>This is car component</div>;

//или если есть больше одной строчки, то надо обернуть в круглые скобки:

/* const car = () => (
    <div>
        This is car component
        <strong>test</strong>
    </div>
) */

// export default car

//Или, можно сразу экспортировать функцию:
//Дбавили return за тем, чтобы сожно было писать больше чем 1 блок кода в функции
// export default (props) => {
    
//     const inputClasses = ["input"];

//     if (props.name !== "") {
//         inputClasses.push("green");

//     } else {
//         inputClasses.push('red');
//     }    

//     if (props.name.length > 4) {
//         inputClasses.push("bold");
//     }

//     const style = {
//         border: "1px solid #ccc",
//         boxShadow: "0 4px 5px 0 rgba(0, 0, 0, .14)"
//     };

//     return (
//         <div className="Car" style={style}>
//             <h3>Car name: {props.name}</h3>
//             <p>Year: <strong>{props.year}</strong></p>
//             <input 
//                 type="text"
//                 onChange={props.onChangeName} 
//                 value={props.name}
//                 className={inputClasses.join(" ")}>    
//             </input>

//             {/* <button onClick={props.onChangeTitle}>Click</button> */}
//             <button onClick={props.onDelete}>Delete</button>
//         </div>
//     )
    
// }

// const Car = props => {
    
//     const inputClasses = ["input"];

//     if (props.name !== "") {
//         inputClasses.push("green");

//     } else {
//         inputClasses.push('red');
//     }    

//     if (props.name.length > 4) {
//         inputClasses.push("bold");
//     }

//     const style = {
//         border: "1px solid #ccc",
//         boxShadow: "0 4px 5px 0 rgba(0, 0, 0, .14)",
//         ":hover": {
//             border: "1px solid #aaa",
//             boxShadow: "0 4px 15px 0 rgba(0, 0, 0, .25)",
//             cursor: "pointer"
//         }
//     };

//     return (
//         <div className="Car" style={style}>
//             <h3>Car name: {props.name}</h3>
//             <p>Year: <strong>{props.year}</strong></p>
//             <input 
//                 type="text"
//                 onChange={props.onChangeName} 
//                 value={props.name}
//                 className={inputClasses.join(" ")}>    
//             </input>

//             {/* <button onClick={props.onChangeTitle}>Click</button> */}
//             <button onClick={props.onDelete}>Delete</button>
//         </div>
//     )
    
// }

class Car extends Component {

    //Еще методы жизненного цикла, а конкретно изменения. Т.е пока мы что-то не изменим, они не будут вызваны.
    
    /* componentWillReceiveProps(nextProps) {
        console.log("Car componentWillReceiveProps", nextProps);
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("Car shouldComponentUpdate", nextProps, nextState);

        return nextProps.name.trim() !== this.props.name.trim();
    }

    componentWillUpdate(nextProps, nextState) {
        console.log("Car componentWillUpdate", nextProps, nextState);
    }

    componentDidUpdate() {
        console.log("Car componentDidUpdate");
    } */

    //Метод жизненного цикла удаления.
    /* componentWillUnmount() {
        console.log("Car componentWillUnmount");
    } */

    //Методы жизненного цикла, появившиеся в React версии 16.3
    //Тоже, что и componentWillUpdate(nextProps, nextState)
    /* static getDerivedStateFromProps(nextProps, prevState) {
        console.log("Car getDerivedStateFromProps", nextProps, prevState);

        return {
            prevState
        };
    }
 
    getSnapshotBeforeUpdate() {
        console.log("Car getSnapshotBeforeUpdate");
    } */

    //Это новый вариант создания референции. componentDidMount() относится к референции тоже
    constructor(props) {
        super(props);

        this.inputRef = React.createRef()
    }

    componentDidMount() {
        if (this.props.index === 1) {
            this.inputRef.current.focus();
        }      
    }

    render() {

        console.log("Car render");

        // if (Math.random() > 0.7) {
        //     throw new Error("Car random failed");
        // }

        const inputClasses = ["input"];

        if (this.props.name !== "") {
            inputClasses.push("green");

        } else {
            inputClasses.push('red');
        }    

        if (this.props.name.length > 4) {
            inputClasses.push("bold");
        }


        //Как можно создать инлайн псевдоэлементы 
        /* const style = {
            border: "1px solid #ccc",
            boxShadow: "0 4px 5px 0 rgba(0, 0, 0, .14)",
            ":hover": {
                border: "1px solid #aaa",
                boxShadow: "0 4px 15px 0 rgba(0, 0, 0, .25)",
                cursor: "pointer"
            }
        }; */

        return (
            <React.Fragment>
                <h3>Car name: {this.props.name}</h3>
                <p>Year: <strong>{this.props.year}</strong></p>
                <input
                    ref={this.inputRef} //ref не отобразится в html
                    type="text"
                    onChange={this.props.onChangeName} 
                    value={this.props.name}
                    className={inputClasses.join(" ")}>    
                </input>

                {/* <button onClick={props.onChangeTitle}>Click</button> */}
                <button onClick={this.props.onDelete}>Delete</button>
            </React.Fragment>
        )
    }
}

//export default Radium(Car);

//Валидация с помощью библиотеки PropTypes
Car.propTypes = {
    name: PropTypes.string.isRequired,
    year: PropTypes.number,
    index: PropTypes.number,
    onChangeName: PropTypes.func,
    onDelete: PropTypes.func
}

export default withClass(Car, "Car");