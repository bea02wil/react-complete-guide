import React from "react";

export default class ErrorBoundary extends React.Component {

    state = {
        hasError: false
    }

    componentDidCatch(error, info) {
        this.setState({hasError: true})
    };

    render() {
        if (this.state.hasError) {
            return <h1 style={{color: "red"}}>Something went wrong</h1>;
        }

        //возвращаем чилдрен т.е содежимое тега <ErrorBoundary>, а содержимое <ErrorBoundary> находится
        //в App.js, т.е вернем наше приложение  
        return this.props.children;
     } 
}