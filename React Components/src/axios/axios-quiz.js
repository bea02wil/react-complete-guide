import axios from "axios";

export default axios.create({
    baseURL: "https://react-quiz-736d0-default-rtdb.europe-west1.firebasedatabase.app/"
})