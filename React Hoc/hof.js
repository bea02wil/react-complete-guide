const numbers = [1, 2, 3, 4, 5];

function createAddNumber(number) {
    return function(array) {
        return array.map(item => item += number);
    }
}

const addOne = createAddNumber(1);

const addFive = createAddNumber(5);

console.log(addOne(numbers));
console.log(addFive(numbers));


// Сверху - это замена кода ниже, чтобы не писать кучу циклов с нужным прибавляемым значением
// const newNumbers = [];

// for (let i = 0; i < numbers.length; i++) {
//     newNumbers.push(numbers[i] + 1);
// }

// for (let i = 0; i < numbers.length; i++) {
//     newNumbers.push(numbers[i] + 2);
// }