import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const starWarsChars = [
  { name: "Дарт Вейдер", side: "dark" },
  { name: "Люк Скайвокер", side: "light" },
  { name: "Палпатин", side: "dark" },
  { name: "Обиван Кеноби", side: "light" },
]

const App = ({list}) => (

  //чтобы не фильтровать по каждому параметру ( а их может быть десятки), то написали hoc функцию
  //const filteredList = list.filter(char => char.side === side);

  <ul>
      { list.map((char, index) => {
        return (
          <li key={char.name + index}>
            <strong>{char.name}</strong> - &nbsp;
            {char.side}
          </li>
        )
      }) }
    </ul>
    
)

const withFilteredProps = Component => ({list, side}) => {
  const filteredList = list.filter(char => char.side === side);

  return <Component list={filteredList}/>
}

const FilteredList = withFilteredProps(App);

ReactDOM.render(<FilteredList list={starWarsChars} side="light"/>, document.getElementById('root'));
